#!/bin/sh

source_dir="/etc/portage/"
destination_dir="/home/ali/portage-configs/"

# 检查源目录和目标目录文件列表是否相同
 diff_output=$(sudo diff -r --exclude=make.profile --exclude=savedconfig "$source_dir" "$destination_dir" --exclude=".git" --exclude="README.md" --exclude="sync-and-update.sh" 2>&1)

if [ -z "$diff_output" ]; then
    notify-send "文件已同步，无需进行任何操作。"
    sudo chown -R ali:ali "$destination_dir" 
else
    notify-send "文件有变动，开始同步..."
    # 执行rsync命令进行同步
    sudo rsync -av --exclude='make.profile' --exclude='savedconfig/' "$source_dir" "$destination_dir"
    sudo chown -R ali:ali "$destination_dir" 
    notify-send "本地文件同步完成,正在同步至远程仓库："
    cd $destination_dir 
    git add .
    git commit -m "Daily sync"
    git push -u origin master
    notify-send "远程仓库同步完成。"
fi

